#ifndef TheP8I_Ropewalk_H
#define TheP8I_Ropewalk_H
//
// This is the declaration of the Ropewalk class.
//
#include "TheP8I/Config/TheP8I.h"
#include "ThePEG/EventRecord/Particle.h"
#include "ThePEG/EventRecord/ColourSinglet.h"
#include "ThePEG/Utilities/UtilityBase.h"
#include "ThePEG/Utilities/Throw.h"

namespace std {

template <>
struct less<ThePEG::ColourSinglet *> :
    public binary_function<ThePEG::ColourSinglet *,
    ThePEG::ColourSinglet *, bool>
{
    /**
     * This is the function called when comparing two pointers to
     * type_info.
     */
    bool operator()(const ThePEG::ColourSinglet * x,
                    const ThePEG::ColourSinglet * y) const {
        if ( x && y ) return x->partons() < y->partons();
        return !y;
    }
};

}

namespace TheP8I {

using namespace ThePEG;

typedef Tension StringTension;


/**
 * Here is the documentation of the Ropewalk class.
 */
class Ropewalk {

public:


    double lambdaBefore;
    std::vector<double> mspec;
    /**
     * Forward declaration of a Dipole.
     */
    struct Dipole;


    /**
     * Container class storing information of a dipole overlapping with another;
     */
    struct OverlappingDipole {

        /**
         * Standard constructor.
         */
        OverlappingDipole(const Dipole & d,
                          const LorentzRotation R, const Ropewalk * rw);

        /**
         * Check if ovelapping at specific rapidity.
         */
        bool overlap(double y, LorentzPoint b1, Length R0) {
            if ( y < std::min(ya, yc) || y > std::max(ya, yc) ) return false;
            LorentzPoint b2 = ba + (bc - ba)*(y - ya)/(yc - ya);
            return (b1 - b2).perp() <= R0;
        }


        LorentzPoint pointAtRap(double y) {
            return ba + (bc - ba)*(y - ya)/(yc - ya);
        }
        /**
         * Pointer to the dipole.
         */
        const Dipole * dipole;

        /**
         * The boosted rapidities.
         */
        double yc, ya;

        /**
         * The propagated and boosted positions.
         */
        LorentzPoint bc, ba;

        /**
         * Relative direction.
         */
        int dir;

        /**
         * Initially estimated fraction of overlap.
         */
        double yfrac;

    };

    /**
     * Container class containing information about overlaps for a dipole.
     * *** ATTENTION *** the meaning of pa and pc is reversed: pa is
     *      always the COLOURED and pc is always the ANTI-COLOURED
     */
    struct Dipole {

        struct DipoleException : public Exception {};


        /**
         * Constructor.
         */
        Dipole(tcPPtr p1, tcPPtr p2, ColourSinglet & str, Energy m0In)
            : pc(p2), pa(p1), string(&str), n(0), m(0), p(1), q(0), nb(0), broken(false), hadr(false), m0(m0In) {
            if(pc->colourLine() != pa->antiColourLine())
                swap(pc,pa);
            LorentzMomentum pcm = pc->momentum();
            LorentzMomentum pam = pa->momentum();

            //if(pcm.x()/GeV == 0 && pcm.y()/GeV == 0 && pcm.z()/GeV == 0 && pcm.e()/GeV == 0)
            //std::cout << pc->id() << " " << pa->id() << std::endl;
            //std::cout << pam.x()/GeV << " " << pam.y()/GeV << " " << pam.z()/GeV << " " << pam.e()/GeV << std::endl;
            if ( (pcm + pam).m2() < 0.000001*GeV2 )
                Throw<DipoleException>()
                        << "Ropewalk found a minisculedipole. This should not happen, "
                        << "but hoping it is just a fluke and throwing away the event."
                        << Exception::eventerror;
            R = Utilities::boostToCM(std::make_pair(&pcm,&pam) );

        }

        /**
         * Return true if this dipole is breakable. Only dipoles between
         * gluons (not belonging to other dipoles which have broken) and
         * which has a mass above 2 GeV can break.
         */
        bool breakable() const {
            return !broken && pc->id() == ParticleID::g && pa->id() == ParticleID::g &&
                   !pc->decayed() && !pa->decayed() && s() > 4.0*GeV2;
        }

        /**
         * Calculate the probability that this dipole should break. Taking
         * into account the number of negative steps in the colour space
         * and the fraction of overlapping dipoles which are able to
         * break.
         */
        double breakupProbability() const;

        void clear() {
            overlaps.clear();
            n = 0, m = 0, p = 1, q = 0, nb = 0, broken = false, hadr = false;
        }

        // Propagate the particles and the excitations
        void propagate(Length deltat, double deltay);

        /**
         * The minimal rapidity in the lab system of the excited dipole ends
         */
        double minRapidity() const {
            //double y1 = 0.5*log(epc->Pplus()/epc->Pminus());
            //double y2 = 0.5*log(epa->Pplus()/epa->Pminus());
            double y1 = epc->momentum().rapidity();
            double y2 = epa->momentum().rapidity();
            return min(y1,y2);

        }
        /**
        * The maximal rapidity in the lab system
        */
        double maxRapidity() const {
            //double y1 = 0.5*log(epc->Pplus()/epc->Pminus());
            //double y2 = 0.5*log(epa->Pplus()/epa->Pminus());
            double y1 = epc->momentum().rapidity();
            double y2 = epa->momentum().rapidity();
            return std::max(y1,y2);
        }

        /*
         * The b-coordinate (in lab frame) of a point
         * between the propagated particles, at lab-rapidity yIn
         */
        LorentzPoint pointAtRap(double yIn) const {
            double ymin = minRapidity();
            double ymax = maxRapidity();
            LorentzPoint bMin, bMax;
            if (pc->momentum().rapidity() == ymin )
                bMin = pc->vertex(), bMax = pa->vertex();
            else
                bMin = pa->vertex(), bMax = pc->vertex();

            // We assume that the rapidity fraction stays the same,
            // as we cannot calculate yIn in dipole rest frame
            return bMin + (bMax - bMin)*(yIn - ymin)/(ymax - ymin);
        }

        /**
         * Recalculate overlaps assuming a position a fraction ry from
         * the coloured parton.
         */
        double reinit(double ry, Length R0, Energy m0);

        /**
         * Set and return the effective multiplet.
         */
        void initMultiplet();
        void initNewMultiplet();
        void initWeightMultiplet();

        /**
         * Calculate the multiplicity given pp and qq;
         */
        static double multiplicity(int pp, int qq) {
            return ( pp < 0 || qq < 0 || pp + qq == 0 )? 0.0:
                   0.5*(pp + 1)*(qq + 1)*(pp + qq + 2);
        }

        /**
         * Calculate the average kappa-enhancement.
         */
        double kappaEnhancement() const {
            // return 1.0;
            return 0.25*(p + q + 3 - p*q/double(p + q));
        }

        /**
         * Calculate the kappa-enhancement in the first break-up.
         */
        double firstKappa() const {
            return 0.25*(2 + 2*p + q);
        }

        /**
         * Calculate boosted kappa-enhancement.
         */
        double firstKappa(double alpha) const {
            if (alpha > 0)
                return alpha*firstKappa();
            return firstKappa();
        }

        /**
         * return the momentum of this dipole
         */
        LorentzMomentum momentum() const {
            return pc->momentum() + pa->momentum();
        }

        /**
         * Return the squared invariant mass of this dipole.
         */
        Energy2 s() const {
            return (pc->momentum() + pa->momentum()).m2();
        }

        /**
         * Return the effective rapidityspan of this dipole.
         */
        double yspan(Energy m0) const {
            return s() > sqr(m0)? 0.5*log(s()/sqr(m0)): 0.0;
        }

        /**
         * The coloured particle.
         */
        mutable tcPPtr pc;

        /**
         * The anti-coloured particle.
         */
        mutable tcPPtr pa;

        /**
         * The coloured, excited particle
         */
        PPtr epc;

        /**
         * The anti-coloured, excited particle
         */
        PPtr epa;

        /**
         * All excitations belonging to this dipole ordered in
         * rapidity in lab system
         */
        typedef std::map<double, PPtr> DipEx;
        DipEx excitations;
        void printExcitations() {
            for(DipEx::iterator itr = excitations.begin(); itr != excitations.end(); ++itr) {
                std::cout << itr->first << " " << itr->second->rapidity() << " " << itr->second->momentum().perp()/GeV << std::endl;
            }
        }

        /**
         * Recoil the dipole from adding a gluon
         * this function will not actually add the gluon, only the
         * recoil, if possible, and return true or false if it was
         */
        bool recoil(const LorentzMomentum& pg, bool dummy = false) {
            // Keep track of direction
            int sign = 1;
            if(epc->rapidity() > epa->rapidity()) sign = -1;

            // lc momenta after inserting the gluon
            Energy pplus = epc->Pplus() + epa->Pplus() - pg.plus();
            Energy pminus = epc->Pminus() + epa->Pminus() - pg.minus();

            // The new lightcone momenta of the dipole ends
            Energy ppa = 0.0*GeV;
            Energy ppc = 0.0*GeV;
            Energy pma = 0.0*GeV;
            Energy pmc = 0.0*GeV;
            Energy2 mta2 = epa->mt2();
            Energy2 mtc2 = epc->mt2();
            Energy mta = ThePEG::sqrt(mta2);
            Energy mtc = ThePEG::sqrt(mtc2);

            if ( pplus*pminus <= sqr(mta + mtc) ||
                    pplus <= ZERO || pminus <= ZERO ) return false;

            Energy4 sqarg = sqr(pplus*pminus + epa->mt2() - epc->mt2()) -
                            4.0*epa->mt2()*pplus*pminus;
            // Calculate the new momenta
            if(sqarg <= ZERO ) return false;
            if(sign > 0) {
                ppa = (pplus*pminus + epa->mt2() - epc->mt2() +
                       ThePEG::sqrt(sqarg))*0.5/pminus;
                pma = epa->mt2()/ppa;
                pmc = pminus - pma;
                ppc = epc->mt2()/pmc;
                if ( ppa*mtc < ppc*mta ) return false; // Check rapidity after.
            }
            else {
                pma = (pplus*pminus + epa->mt2() - epc->mt2() +
                       ThePEG::sqrt(sqarg))*0.5/pplus;
                ppa = epa->mt2()/pma;
                ppc = pplus - ppa;
                pmc = epc->mt2()/ppc;
                if ( ppa*mtc > ppc*mta ) return false; // Check rapidity after.
            }

            LorentzMomentum shifta = LorentzMomentum(epa->momentum().x(),epa->momentum().y(),
                                     0.5*(ppa - pma),0.5*(ppa + pma));
            LorentzMomentum shiftc = LorentzMomentum(epc->momentum().x(), epc->momentum().y(),
                                     0.5*(ppc - pmc), 0.5*(ppc + pmc));
            // Assign the new momenta
            if(!dummy) {
                epa->setMomentum(shifta);
                epc->setMomentum(shiftc);
            }
            return true;

        }

        /**
         * Insert an excitation provided that it is not already there
         */
        void addExcitation(double ylab, PPtr ex) {
            auto ret = excitations.equal_range(ylab);
            for(auto itr = ret.first; itr != ret.second; ++itr )
                if(ex == itr->second) {
                    return;
                }
            excitations.insert(std::make_pair(ylab,ex));
        }
        /**
         * Add all excitations to the dipole
         */
        void excitationsToString(bool gluonMass, double yCutOff);

        /**
         * Extract the distance between all adjecant excitation pairs on a dipole
         */
        std::multimap< double, std::pair<double, double> > getDistPairs();

        /**
        * Return the current step.
        */
        Step & step() const;

        /**
        * The propagated and boosted positions.
        */
        LorentzPoint bc, ba;

        /**
         * The Lorentz rotation taking us to this dipole's rest system
         */
        LorentzRotation R;

        /**
         * The overlapping dipoles with the amount of overlap (negative
         * means anti overlap).
         */
        std::vector<OverlappingDipole> overlaps;


        /**
         * The string to which this Dipole belongs.
         */
        ColourSinglet * string;

        /**
         * The summed parallel (n) and anti-parallel (m) overlaps from
         * other dipoles.
         */
        int n, m;

        /**
         * The multiplet.
         */
        int p, q;

        /**
         * The number of baryons from junctions
         */
        int nb;



        /**
         * Indicate that this dipole has been broken.
         */
        mutable bool broken;
        mutable bool hadr;
        /**
        * The m0 parameter
        */
        Energy m0;
    };


    /**
     * Convenient typedef
     */
    typedef std::map<ColourSinglet *, std::vector<Dipole *> > DipoleMap;

public:

    /** @name Standard constructors and destructors. */
    //@{
    /**
     * The default constructor.
     */
    Ropewalk(const std::vector<ColourSinglet> & singlets, Length width,
             Energy scale, double jDC, bool throwaway = true, bool shoving = true, double rcIn = 5.0, StringTension gaIn = 1.0*GeV/femtometer, double geIn = 1.0, double ycIn = 4.0, double dyIn = 0.1, Length tsIn = 1.0*femtometer, Length dtIn = 1.0*femtometer, bool lorentzIn = true, bool cylinderIn = false, Energy ptCutIn = 3.0*GeV,
             double mstringyIn = 8.0, bool longSoftIn = false, bool deb = false);

    /**
     * The destructor.
     */
    virtual ~Ropewalk();
    //@}

    /**
     * Return all ColourSinglet objects with their kappa enhancement.
     */
    std::vector< std::pair<ColourSinglet,double> > getSinglets(double DeltaY = 0.0) const;

    /**
     * Calculate the average kappa-enhancement for the given colour
     * singlet, optionally only taking into account dipoles the
     * rapidity interval |y| < deltaY. Note that \a cs must point to a
     * object in the stringdipoles map. @return -1 if cs is not found.
     */
    double averageKappaEnhancement(ColourSinglet * cs, double deltaY = 0.0) const {
        DipoleMap::const_iterator it = stringdipoles.find(cs);
        if ( it == stringdipoles.end() ) return -1.0;
        return averageKappaEnhancement(it, deltaY);
    }

    /**
     * Calculate the average kappa-enhancement for the given iterator
     * pointing into the stringdipoles map, optionally only taking
     * into account dipoles the rapidity interval |y| < deltaY. Note
     * that \a cs must point to a object in the stringdipoles
     * map. @return -1 if something went wrong.
     */
    double averageKappaEnhancement(DipoleMap::const_iterator it,
                                   double deltaY = 0.0) const {
        return averageKappaEnhancement(it->second, deltaY);
    }

    /**
    * Calculate the average kappa-enhancement the given vector of
    * dipoles, optionally only taking into account dipoles the
    * rapidity interval |y| < deltaY. @return -1 if something went
    * wrong.
    */
    double averageKappaEnhancement(const std::vector<Dipole *> & dipoles,
                                   double deltaY = 0.0) const;

    /**
     * Propagate a parton a finite time inthe lab-system.
     */
    LorentzPoint propagate(const LorentzPoint & b,
                           const LorentzMomentum & p) const;

    /**
    / Get number of juctions
    */
    double getNb();

    double getkW();
    /**
    / Get m,n for all strings
    */
    std::pair<double,double> getMN();


    /**
    / Get lambda measure for event
    */
    double lambdaSum(double cutoff = 0.1);

    /**
     * Calculate the rapidity of a Lorentz vector but limit the
     * transverse mass to be above m0.
     */
    double limitrap(const LorentzMomentum & p) const;

    /**
     * Sort the given vector of dipoles so that the particle \a p is
     * first and the others follows in sequence.  If \a p is a
     * (anti-)triplet, it will be the pc(pa) of the first dipole, and
     * the pc(pa) of the second will be the pa(pc) of the first and so
     * on. If \a p is an octet, the direction will be as if it was a
     * triplet.
     */
    static void sort(std::vector<Dipole *> & dips, tcPPtr p);

protected:

    /**
     * Extract all dipoles.
     */
    void getDipoles();

    /**
     * Calculate all overlaps and initialize multiplets in all dipoles.
     */
    void calculateOverlaps(bool doPropagate);

    /**
     * Break dipoles (and strings)
     */
    void doBreakups();

    /**
     * Shove the dipoles away from each other
     */
    void shoveTheDipoles();

    /**
     * Return the current step.
     */
    Step & step() const;

    /**
     * Make a copy of a ColourSinglet making sure all partons are final.
     */
    ColourSinglet * cloneToFinal(const ColourSinglet & cs);

public:

    DipoleMap::iterator begin() {
        return stringdipoles.begin();
    }

    DipoleMap::iterator end() {
        return stringdipoles.end();
    }

    DipoleMap::iterator beginSpectators() {
        return spectatorDipoles.begin();
    }

    DipoleMap::iterator endSpectators() {
        return spectatorDipoles.end();
    }

    /**
     * Exception class.
     */
    struct ColourException: public Exception {};

private:

    /**
     * The assumed radius of a string.
     */
    Length R0;

    /**
     * The assumed mass for calculating rapidities
     */
    Energy m0;

    /**
     * The junction colour fluctuation parameter
     */
    double junctionDiquarkProb;

    /**
     * Cutoff distance where we no longer consider
     * shoving
     */
    Length rCutOff;

    /**
     * Amplitude af shoving function
     */
    StringTension gAmplitude;

    /**
     * Shoving function (inverse) multiplier
     */
    double gExponent;

    /**
     * Cutoff in effective rapidity span when clustering
     */
    double yCutOff;

    /**
     * Rapidity slicing for shoving
     */
    double deltay;

    /**
     * Shoving time
     */
    Length tshove;

    /**
     * Shoving time slicing
     */
    Length deltat;




    /**
     * The vector of all Dipoles
     */
    std::vector<Dipole> dipoles;

    /**
     * All participating Dipoles in all string
     */
    DipoleMap stringdipoles;

    /**
     * Spectating dipoles
     */
    DipoleMap spectatorDipoles;

    /**
     * Lorentz contraction of fields
     */
    bool lorentz;

    bool cylinderShove;

    // For keeping only soft long strings
    Energy stringpTCut;

    double minStringy;

    bool longSoft;

    /**
     * Debug flag.
     */
    bool debug;

public:

    mutable double strl0;
    mutable double strl;
    mutable double avh;
    mutable double avp;
    mutable double avq;


private:

    /**
     * The assignment operator is private and must never be called.
     * In fact, it should not even be implemented.
     */
    Ropewalk & operator=(const Ropewalk &) = delete;

    double nb;
};

}

#endif /* TheP8I_Ropewalk_H */

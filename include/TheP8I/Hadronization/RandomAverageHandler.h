#ifndef THEP8I_RandomAverageHandler_H
#define THEP8I_RandomAverageHandler_H

#include "StringPipe.h"

namespace TheP8I {

using namespace ThePEG;

class RandomAverageHandler {
public:
    RandomAverageHandler(bool throwaway);

    ~RandomAverageHandler();

    double KappaEnhancement(StringPipe& pipe);

    void SetEvent(std::vector<StringPipe> event);

    void AddPipe(StringPipe& pipe);

    void RecalculateOverlaps();

    bool RemovePipe(StringPipe& pipe);

    void clear();

private:
    std::vector<StringPipe> _pipes;
};
}
#endif
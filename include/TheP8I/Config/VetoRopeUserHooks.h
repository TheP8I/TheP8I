#ifndef Pythia8_VetoRopeUserHooks_H
#define Pythia8_VetoRopeUserHooks_H

#include "Pythia8/Pythia.h"
#include "Pythia8/Event.h"
#include "ParameterMapper.h"
#include <exception>

namespace Pythia8 {

class VetoRopeUserHooks : public UserHooks {

// Convenient typedefs
    typedef std::map<std::string,double> PytPars;

public:
    // Exception class
    class RopeException: public exception {
    public:
        RopeException(std::string msg) : _msg(msg) { }
        virtual ~RopeException() throw() {}
        virtual const char* what() const throw() {
            return _msg.c_str();
        }

    private:
        std::string _msg;
    };


public:


    VetoRopeUserHooks() : _ph(NULL), _h(-1.0), _deltaY(0.5), _alpha(0.10), _kappaTrunk(10.0), _event(NULL), _stringZ(NULL), debugweight(0.0), nhad(0.0) {

    }

    ~VetoRopeUserHooks() {
    }

    virtual bool canChangeFragPar() {
        return true;
    }

    virtual bool doChangeFragPar(StringFlav* flavPtr, StringZ* zPtr,
                                 StringPT * pTPtr, int endFlavour, double m2Had, std::vector<int> iParton) {
        _stringZ = zPtr;
        _endFlavour = endFlavour;
        _m2Had = m2Had;
        _iParton = iParton;
        try {
            newPar = fetchParameters(endFlavour, m2Had, iParton, 1.0);
            if(newPar.find("null") != newPar.end())
                throw RopeException("Failed to fetch parameters! No Ropes.");
            // Change settings to new settings
            for(PytPars::iterator itr = newPar.begin(); itr!=newPar.end(); ++itr)
                settingsPtr->parm(itr->first,itr->second);
            // Re-initialize flavour, z, and pT selection with new settings
            flavPtr->init(*settingsPtr,rndmPtr);
            zPtr->init(*settingsPtr,*particleDataPtr,rndmPtr);
            pTPtr->init(*settingsPtr,*particleDataPtr,rndmPtr);
            //settingsPtr->listChanged();
        }
        catch(RopeException& e) {
            std::cerr << e.what() << std::endl;
            return false;
        }
        return true;
    }

    virtual bool canChangeFragFunc() {
        if( gluonLoop)
            return false;
        return true;
    }

    double zMax(double kappa, double mT2, bool isDiquark) {
        double a = _ph->getEffectiveA(kappa, mT2, isDiquark);
        double bm2 = _ph->getEffectiveB(kappa);
        bm2 *= mT2;
        double zm = (-bm2-1+sqrt(4*a*bm2+bm2*bm2-2*bm2+1)) / (2*(a-1));
        if(zm <= 1 && zm >= 0)
            return zm;
        zm = (-bm2-1-sqrt(4*a*bm2+bm2*bm2-2*bm2+1)) / (2*(a-1));
        if(zm <= 1 && zm >= 0)
            return zm;
        throw RopeException("Could not calculate minimum of fragmentation function!");
    }

    double fVal(double kappa, double z, double mT2, bool isDiquark) {
        double a = _ph->getEffectiveA(kappa, mT2, isDiquark);
        double bm2 = _ph->getEffectiveB(kappa);
        bm2 *= mT2;
        return std::pow(1.0-z,a)*std::exp(-bm2/z)/z;
    }

    virtual double alternativeZFrag(int idOld, int idNew, double mT2) {
        double zFrag = 0;
        try {
            zFrag = calculateAlternativeZFrag(idOld, idNew, mT2);
        }
        catch(RopeException &e) {
            std::cerr << e.what() << std::endl;
            bool isOldDiquark = (std::abs(pIdold) > 1000 && std::abs(pIdold) < 10000);
            bool isNewDiquark = (std::abs(pIdnew) > 1000 && std::abs(pIdnew) < 10000);
            bool isDiquark = (isOldDiquark || isNewDiquark);

            zFrag = _stringZ->zLund(_ph->getEffectiveA(1.0, mT2, isDiquark), _ph->getEffectiveB(1.0)*mT2);
        }
        return zFrag;

    }

    double calculateAlternativeZFrag(int idOld, int idNew, double mT2) {
        // save stuff for later
        pIdold = idOld;
        pIdnew = idNew;

        // Find if old or new flavours correspond to diquarks.
        bool isOldDiquark = (std::abs(pIdold) > 1000 && std::abs(pIdold) < 10000);
        bool isNewDiquark = (std::abs(pIdnew) > 1000 && std::abs(pIdnew) < 10000);
        bool isDiquark = (isOldDiquark || isNewDiquark);

        sumFF.clear();
        sumFF.push_back(1.0);
        sumFF.push_back(_kappaTrunk);
        size_t s1, s2;
        // Determine how many fragmentation functions we need to put on top of each other
        // before we cover everything
        do {
            s1 = sumFF.size();
            for(size_t i = 0; i+1 != s1; ++i) {
                double kappaPrime = (sumFF[i] + sumFF[i+1])/2;
                double zPrimeMax = zMax(kappaPrime, mT2, isDiquark);
                double fPrime = fVal(kappaPrime, zPrimeMax, mT2, isDiquark);
                double fLarge = 0;
                for(size_t j = 0; j!= s1; ++j)
                    fLarge += fVal(sumFF[j],zPrimeMax,mT2,isDiquark);
                if (fPrime > fLarge) {
                    sumFF.push_back(kappaPrime);
                }
            }
            s2 = sumFF.size();
        } while(s2 > s1);

        // We now select one of them randomly
        size_t index = (double)s2*rndmPtr->flat();
        aEst = _ph->getEffectiveA(sumFF.at(index), mT2, isDiquark);
        bEst = _ph->getEffectiveB(sumFF.at(index));
        bEst *= mT2;
        zEst = _stringZ->zLund(aEst,bEst);

        return zEst;
    }


    virtual bool doVetoFragmentation(Particle p) {
        if (gluonLoop)
            return false;
        double mT2 =  p.mT2();
        PytPars pNow = fetchParameters(_endFlavour, _m2Had, _iParton, 1.0);

        double rhoNow = pNow.find("StringFlav:probStoUD")->second;
        double xiNow = pNow.find("StringFlav:probQQtoQ")->second;
        double xNow =  pNow.find("StringFlav:probSQtoQQ")->second;
        double yNow =  pNow.find("StringFlav:probQQ1toQQ0")->second;
        double sigmaNow =  pNow.find("StringPT:sigma")->second;

        double rhoEst = newPar.find("StringFlav:probStoUD")->second;
        double xiEst = newPar.find("StringFlav:probQQtoQ")->second;
        double xEst =  newPar.find("StringFlav:probSQtoQQ")->second;
        double yEst =  newPar.find("StringFlav:probQQ1toQQ0")->second;
        double sigmaEst =  newPar.find("StringPT:sigma")->second;

        // Find if old or new flavours correspond to diquarks.
        bool isOldDiquark = (std::abs(pIdold) > 1000 && std::abs(pIdold) < 10000);
        bool isNewDiquark = (std::abs(pIdnew) > 1000 && std::abs(pIdnew) < 10000);
        bool isDiquark = (isOldDiquark || isNewDiquark);

        double aNow = _ph->getEffectiveA(_enh,mT2,isDiquark);
        double bNow = _ph->getEffectiveB(_enh);
        // We build up the weight
        double w1 = rhoNow/rhoEst;
        if(!isDiquark)
            w1*=xiNow*yNow*xNow/xiEst/yEst/xEst;
        double w2 = std::pow(sigmaNow/sigmaEst,2)*std::exp(-0.5*p.pT2()*(1.0/sigmaNow/sigmaNow - 1.0/sigmaEst/sigmaEst));
        double fw = 0;
        for (size_t i = 0; i < sumFF.size(); ++i) {
            double ai = _ph->getEffectiveA(sumFF.at(i),mT2,isDiquark);
            double bi = _ph->getEffectiveB(sumFF.at(i));
            fw += std::pow(1-zEst,ai)/zEst*std::exp(-bi*mT2/zEst);
        }

        fw = std::pow(1-zEst,aNow)/zEst*std::exp(-bNow*mT2/zEst)/fw;

//   if(w1 > 1.0 || w2 > 1 || fw > 1)
//     cout << "w1 = " << w1 << " w2 = " << w2 << " fw = " << fw << endl;
//    cout << " w = " << w1*w2*fw << endl;
        debugweight=w1*w2*fw;
        std::cout << debugweight << std::endl;
        if(rndmPtr->flat() < w1*w2*fw)
            return false;
        return true;
    }

    void setParameterMapper(ParameterMapper * ph) {
        _ph = ph;
    }

    void setEvent(Event& event) {
        _event = &event;
    }

    // Update event, needs to be done from main file
    void newEvent() {
        _hadronized.clear();
    }

    // Set enhancement manually from main file
    // Will overrule others if set.
    void setEnhancement(double h) {
        _h = h;
    }

    // Set the rap. span around a break to look for
    // overlapping gluons
    void setRapiditySpan(double dy) {
        _deltaY = dy;
    }

    // Set the ratio of string radius to
    // radius of a pp collision
    void setRadiusRatio(double alpha) {
        _alpha = alpha;
    }

private:

    PytPars fetchParameters(int endFlavour, double m2Had, std::vector<int> iParton, double multiplier=1.0) {
        if(!_event)
            throw RopeException("Can't find event!");

        if(!(_ph))
            throw RopeException("Missing parameter handler!");

        if(find(_hadronized.begin(),_hadronized.end(),*iParton.begin()) == _hadronized.end()) {
            _hadronized.reserve(_hadronized.size() + iParton.size());
            _hadronized.insert(_hadronized.end(),iParton.begin(),iParton.end());
        }

        // If we have manually set enhancement, we should just use that, and avoid complicated behavior.
        if(_h > 0) return _ph->getEffectiveParameters(_h);

        // Quark string ends, default mode
        if (endFlavour != 21) {
            gluonLoop = false;
            // Test consistency
            if(_event->at(*(iParton.begin())).id() != endFlavour &&
                    _event->at(*(iParton.end() - 1)).id() != endFlavour)
                throw RopeException("Quark end inconsistency!");

            // First we must let the string vector point in the right direction
            if(_event->at(*(iParton.begin())).id() != endFlavour)
                reverse(iParton.begin(),iParton.end());

            // Initialize a bit
            Vec4 hadronic4Momentum(0,0,0,0);
            _enh = 1.0;
            double dipFrac;
            std::vector<int>::iterator dipItr;
            // Find out when invariant mass exceeds m2Had
            for(dipItr = iParton.begin(); dipItr != iParton.end(); ++dipItr) {
                double m2Big = hadronic4Momentum.m2Calc();
                if( m2Had <= m2Big) {
                    // Approximate the fraction we are in on the dipole, this goes in three cases
                    // We are at the beginning
                    if(m2Had == 0) {
                        dipFrac = 0;
                    }
                    // We are somewhere in the first dipole
                    else if(dipItr - 1  == iParton.begin()) {
                        dipFrac = std::sqrt(m2Had/m2Big);
                    }
                    else {
                        if(_event->at(*(dipItr - 1)).id() != 21)
                            throw RopeException("Connecting partons should always be gluons");

                        hadronic4Momentum -= 0.5*_event->at(*(dipItr -1)).p();
                        double m2Small = hadronic4Momentum.m2Calc();

                        dipFrac = (std::sqrt(m2Had) - std::sqrt(m2Small)) / (std::sqrt(m2Big) - std::sqrt(m2Small));
                    }
                    break;
                }
                hadronic4Momentum += _event->at(*dipItr).id() == 21 ? 0.5*_event->at(*dipItr).p() : _event->at(*dipItr).p();
            }
            // If we reached the end
            // we are in a small string that should just be collapsed
            if(dipItr == iParton.end())
                return _ph->getEffectiveParameters(1.0);
            // Sanity check
            if(dipFrac < 0 || dipFrac > 1)
                throw RopeException("We can never be less than 0 or more than 100 perc. in on a dipole.");
            // We now figure out at what rapidity value,
            // in the lab system, the string is breaking
            double yBreak;
            // Trivial case, just inherit
            if(dipFrac == 0)
                yBreak = _event->at(*dipItr).y();
            else {
                // Sanity check
                if(dipItr == iParton.begin())
                    throw RopeException("We are somehow before the first dipole.");
                double dy = _event->at(*dipItr).y() - _event->at(*(dipItr - 1)).y();
                yBreak = _event->at(*(dipItr - 1)).y() + dipFrac*dy;
            }
            // Count the number of partons in the whole
            // event within deltay of breaking point
            double p = 1;
            double q = 0;

            for(int i = 0; i < _event->size(); ++i) {
                // Don't double count partons from this
                // string (no self-overlap)
                if(find(iParton.begin(),iParton.end(), i) != iParton.end())
                    continue;
                // Don't count strings that are already hadronized
                if(find(_hadronized.begin(),_hadronized.end(),i) != _hadronized.end())
                    continue;
                double pRap = _event->at(i).y();
                if(pRap > yBreak - _deltaY && pRap < yBreak + _deltaY ) {
                    // Do a "Buffon" selection to decide whether
                    // two strings overlap in impact parameter space
                    // given ratio of string diameter to collision diameter (_alpha)
                    double r1 = rndmPtr->flat();
                    double r2 = rndmPtr->flat();
                    double theta1 = 2*M_PI*rndmPtr->flat();
                    double theta2 = 2*M_PI*rndmPtr->flat();
                    // Overlap?
                    if(4*_alpha*_alpha > pow2(sqrt(r1)*std::cos(theta1) - std::sqrt(r2)*std::cos(theta2)) +
                            pow2(std::sqrt(r1)*std::sin(theta1) - std::sqrt(r2)*std::sin(theta2))) {
                        if(rndmPtr->flat() < 0.5) p += 0.5;
                        else q += 0.5;
                    }
                }
            }
            _enh = 0.25*(2.0*p+q+2.0);

            return _ph->getEffectiveParameters(multiplier*_enh);
        }
        // For closed gluon loops we cannot distinguish the ends.
        // Do nothing instead
        else {
            gluonLoop = true;
            return _ph->getEffectiveParameters(1.0);
        }

        return _ph->getEffectiveParameters(1.0);
    }
    ParameterMapper* _ph;
    double _h, _deltaY, _alpha, _kappaTrunk, _enh;
    double zEst, aEst, bEst;
    Event*  _event;
    std::vector<int> _hadronized;
    int pIdold, pIdnew;
    StringZ* _stringZ;
    PytPars newPar;
    int _endFlavour;
    double _m2Had;
    std::vector<int> _iParton;
    std::vector<double> sumFF;
    bool gluonLoop;
    double debugweight;
    double nhad;
};

}
#endif

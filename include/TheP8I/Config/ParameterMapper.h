#include "Pythia8/Pythia.h"
#include <exception>
#include <string>

#ifndef Pythia8_ParameterMapper_H
#define Pythia8_ParameterMapper_H

class ParameterMapper {

// Convenient typedefs

    typedef std::map<std::string,double> PytPars;

public:
    // Exception class
    class ParameterMapException: public exception {
    public:
        ParameterMapException(std::string msg) : _msg(msg) { }
        virtual ~ParameterMapException() throw() {}
        virtual const char* what() const throw() {
            return _msg.c_str();
        }

    private:
        std::string _msg;
    };

public:

    ParameterMapper(PytPars pars) {
        _beta = 0.2;
        _parameters.clear();

        try {
            // Initialize default values from input
            const int len = 8;
            std::string parameters [len] = {"StringPT:sigma", "StringZ:aLund", "StringZ:aExtraDiquark",
                                            "StringZ:bLund", "StringFlav:probStoUD", "StringFlav:probSQtoQQ", "StringFlav:probQQ1toQQ0", "StringFlav:probQQtoQ"
                                           };
            double* variables [len] = {&sigma_in, &a_in, &adiq_in, &b_in, &rho_in, &x_in, &y_in, &xi_in};
            for(int i = 0; i < len; ++i) {
                pars.count(parameters[i]) ? *variables[i] = pars.find(parameters[i])->second : throw ParameterMapException(parameters[i]);
            }
        }
        catch(ParameterMapException& e) {
            // We could do some insertion of hardcoded defaults here. Now we just fail...
            std::string s1 = "Failed to initialize parameter: ";
            std::string s2 = e.what();
            throw ParameterMapException(s1 + s2);
        }
        // Insert the kappa = 1 case so we don't have to calculate it later
        sigma_eff = sigma_in, b_eff = b_in, rho_eff = rho_in, x_eff = x_in, y_eff = y_in, xi_eff = xi_in;
        if(!insertEffectiveParameters(1.0))
            throw ParameterMapException("Failed inserting default values into mapper");
        // Sanity check of parameters
        if (rho_in < 0 || rho_in > 1 || x_in < 0 || x_in > 1 || y_in < 0 || y_in > 1 || xi_in < 0 ||
                xi_in > 1 || sigma_in < 0 || sigma_in > 1 || a_in < 0.0 || a_in > 2.0 || b_in < 0.2 || b_in > 2.0) {
            throw ParameterMapException("Did not recieve sane initial Pythia8 hadronization values");
        }

    }

    virtual ~ParameterMapper() { };


    PytPars getEffectiveParameters(double h) {
        std::map<double,PytPars>::iterator it = _parameters.find(h);
        if(it!=_parameters.end()) return it->second;

        if(!calculateEffectiveParameters(h))
            throw ParameterMapException("Something went wrong calculating effective Pythia parameters!");

        if(!insertEffectiveParameters(h))
            throw ParameterMapException("Something went wrong inserting effective Pythia parameters!");

        return getEffectiveParameters(h);
    }

    double getEffectiveA(double h, double mT2, bool isDiquark) {
        if (h == _hsave && mT2 == _mT2save)
            return _asave;
        _hsave = h;
        _mT2save = mT2;
        double thisb = getEffectiveParameters(h).find("StringZ:bLund")->second;
        double a_eff = aEffective(a_in,thisb,mT2);
        if(!isDiquark) {
            _asave = a_eff;
            return a_eff;
        }
        a_eff = aEffective(a_in + adiq_in,thisb,mT2);
        _asave = a_eff;
        return a_eff;
    }

    double getEffectiveB(double h) {
        return getEffectiveParameters(h).find("StringZ:bLund")->second;
    }

protected:

private:

    bool insertEffectiveParameters(double h) {
        PytPars p;
        p.insert(std::make_pair<const std::string,double>("StringPT:sigma",sigma_eff));
        p.insert(std::make_pair<const std::string,double>("StringZ:bLund",b_eff));
        p.insert(std::make_pair<const std::string,double>("StringFlav:probStoUD",rho_eff));
        p.insert(std::make_pair<const std::string,double>("StringFlav:probSQtoQQ",x_eff));
        p.insert(std::make_pair<const std::string,double>("StringFlav:probQQ1toQQ0",y_eff));
        p.insert(std::make_pair<const std::string,double>("StringFlav:probQQtoQ",xi_eff));
        return (_parameters.insert(std::make_pair<double,PytPars>(h,p)).second);
    }

    bool calculateEffectiveParameters(double h) {
        if (h <= 0) return false;

        double hinv = 1.0 / h;

        rho_eff = std::pow(rho_in, hinv);
        x_eff = std::pow(x_in, hinv);
        y_eff = std::pow(y_in, hinv);
        sigma_eff = sigma_in * std::sqrt(h);

        double alpha = (1 + 2*x_in*rho_in + 9*y_in + 6*x_in*rho_in*y_in + 3*y_in*x_in*x_in*rho_in*rho_in)/(2 + rho_in);
        double alpha_eff = (1 + 2*x_eff*rho_eff + 9*y_eff +
                            6*x_eff*rho_eff*y_eff + 3*y_eff*x_eff*x_eff*rho_eff*rho_eff)/(2 + rho_eff);

        xi_eff = alpha_eff*_beta*std::pow(xi_in/alpha/_beta,hinv);

        if (xi_eff > 1.0) xi_eff = 1.0;

        b_eff = (2 + rho_eff)/(2 + rho_in) * b_in;
        if (b_eff < 0.2) b_eff = 0.2;
        if (b_eff > 2.0) b_eff = 2.0;

        return true;
    }

    double aEffective(double a_orig, double thisb, double mT2) {
        double N = integrateFragFun(a_orig, b_in, mT2);
        double N_eff = integrateFragFun(a_orig, thisb, mT2);
        int s = (N - N_eff) < 0 ? -1 : 1;
        double da = 0.1;
        double a_new = a_orig - s * da;
        do {
            N_eff = integrateFragFun(a_new, thisb, mT2);
            if (((N - N_eff) < 0 ? -1 : 1) != s) {
                s = (N - N_eff) < 0 ? -1 : 1;
                da /= 10.0;
            }
            a_new -= s * da;
            if (a_new < 0.0) {a_new = 0.0; break;}
            if (a_new > 2.0) {a_new = 2.0; break;}
        } while (da > 0.001);
        return a_new;
    }


    double fragf(double z, double a, double b, double mT2) {
        // The Lund Fragmentation function with a cutoff for low z
        // @todo: Does a global small number exist in the program?
        if(z < 1.0/10000.0)
            return 0.0;
        return std::pow(1 - z, a) * std::exp(-b*mT2/z) / z;
    }

    double trapIntegrate(double a, double b, double mT2, double sOld, int n) {
// Compute the nth correction to the integral of fragfunc between 0 and 1
// using extended trapezoidal rule
        if(n == 1)
            return 0.5*(fragf(0.0,a,b,mT2)+fragf(1.0,a,b,mT2));
        // We want 2^(n-2) interior points
        int intp = 1;
        intp <<= n-2;
        double deltaz = 1.0/((double)intp);
        double z = 0.5 * deltaz;
        double sum = 0.0;
        // Do the integral
        for (int i = 0; i < intp; ++i, z+=deltaz)
            sum+=fragf(z,a,b,mT2);
        return 0.5*(sOld+sum/(double)intp);
    }

    double integrateFragFun(double a, double b, double mT2) {
        // Using Simpson's rule to integrate the Lund fragmentation function
        double nextIter, nextComb;
        double thisComb = 0.0, thisIter = 0.0;
        double error = 1.0e-3;
        // @todo 20 is the max number of iterations, 3 is min. Is this good enough? Could be estimatated on the fly?
        for(int i=1; i<20; ++i) {
            nextIter = trapIntegrate(a,b,mT2,thisIter,i);
            nextComb = (4.0*nextIter-thisIter)/3.0;
            if(i > 3)
                if(abs(nextComb - thisComb) < error*abs(nextComb)) {
                    return nextComb;
                }
            thisIter = nextIter;
            thisComb = nextComb;
        }
        throw ParameterMapException("Could not get Simpson's rule integral to converge");
    }

    // sets of parameters, ordered in h
    std::map<double, PytPars> _parameters;

    // Initial values of parameters (to be mapped)
    double a_in, adiq_in, b_in, rho_in, x_in, y_in, xi_in, sigma_in;
    // Effective values of parameters (after mapping)
    double b_eff, rho_eff, x_eff, y_eff, xi_eff, sigma_eff;
    // Junction parameter
    double _beta;
    // Save previous a value in buffer
    double _hsave, _mT2save, _asave;
};
#endif

#ifndef THEP8I_H
#define THEP8I_H

/** \file TheP8I.h This is the main config header file for the TheP8I
 * interface of Pythia8 to the ThePEG. All classes using TheP8I
 * classes should include this file directly or indirectly.
 */

#include "ThePEG/Config/ThePEG.h"

/**
 * This is the main namespace which all identifiers in TheP8I are
 * declared. All identifiers from the ThePEG namespace are available.
 */
namespace TheP8I {
using namespace ThePEG;
}

#endif /* THEP8I_H */


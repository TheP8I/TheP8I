#if PYTHIA_VERSION_INTEGER==8243
  static Switch<BoseEinsteinHandler,bool> interfaceBoseEinstein_Pion
    ("BoseEinstein_Pion",
     "  Include effects or not for identical \\f$pi^+\\f$, \\f$pi^-\\f$  and \\f$pi^0\\f$.  ",
     &BoseEinsteinHandler::theBoseEinstein_Pion, true, true, false);
  static SwitchOption interfaceBoseEinstein_PionOn
    (interfaceBoseEinstein_Pion,
     "On",
     "On",
     true);
  static SwitchOption interfaceBoseEinstein_PionOff
    (interfaceBoseEinstein_Pion,
     "Off",
     "Off",
     false);

  static Switch<BoseEinsteinHandler,bool> interfaceBoseEinstein_Kaon
    ("BoseEinstein_Kaon",
     "  Include effects or not for identical \\f$K^+\\f$, \\f$K^-\\f$,  \\f$K_S^0\\f$ and \\f$K_L^0\\f$.  ",
     &BoseEinsteinHandler::theBoseEinstein_Kaon, true, true, false);
  static SwitchOption interfaceBoseEinstein_KaonOn
    (interfaceBoseEinstein_Kaon,
     "On",
     "On",
     true);
  static SwitchOption interfaceBoseEinstein_KaonOff
    (interfaceBoseEinstein_Kaon,
     "Off",
     "Off",
     false);

  static Switch<BoseEinsteinHandler,bool> interfaceBoseEinstein_Eta
    ("BoseEinstein_Eta",
     "  Include effects or not for identical \\f$eta\\f$ and \\f$eta'\\f$.  ",
     &BoseEinsteinHandler::theBoseEinstein_Eta, true, true, false);
  static SwitchOption interfaceBoseEinstein_EtaOn
    (interfaceBoseEinstein_Eta,
     "On",
     "On",
     true);
  static SwitchOption interfaceBoseEinstein_EtaOff
    (interfaceBoseEinstein_Eta,
     "Off",
     "Off",
     false);

  static Parameter<BoseEinsteinHandler,double> interfaceBoseEinstein_lambda
    ("BoseEinstein_lambda",
     "  The strength parameter for Bose-Einstein effects. On physical grounds  it should not be above unity, but imperfections in the formalism  used may require that nevertheless.  ",
     &BoseEinsteinHandler::theBoseEinstein_lambda, 1., 0., 2.,
     true, false, Interface::limited);

  static Parameter<BoseEinsteinHandler,double> interfaceBoseEinstein_QRef
    ("BoseEinstein_QRef",
     "  The size parameter of the region in \\f$Q\\f$ space over which  Bose-Einstein effects are significant.  Can be thought of as  the inverse of an effective distance in normal space,  \\f$R = hbar / QRef\\f$, with \\f$R\\f$ as used in the above equation.  That is, \\f$f_2(Q) = (1 + lambda * exp(-(Q/QRef)^2)) * (...)\\f$.  ",
     &BoseEinsteinHandler::theBoseEinstein_QRef, 0.2, 0.05, 1.,
     true, false, Interface::limited);

  static Parameter<BoseEinsteinHandler,double> interfaceBoseEinstein_widthSep
    ("BoseEinstein_widthSep",
     "  Particle species with a width above this value (in GeV) are assumed  to be so short-lived that they decay before Bose-Einstein effects  are considered, while otherwise they do not. In the former case the  decay products thus can obtain shifted momenta, in the latter not.  The default has been picked such that both \\f$rho\\f$ and  \\f$K^*\\f$ decay products would be modified.  ",
     &BoseEinsteinHandler::theBoseEinstein_widthSep, 0.02, 0.001, 1.,
     true, false, Interface::limited);

  static ParVector<BoseEinsteinHandler,string> interfaceAdditionalP8Settings
    ("AdditionalP8Settings",
     "Each string in this vector will be sent to the Pythia8 Settings "
     "object using the Pythia::readString function. Note that most relevant "
     "settings are already available as normal ThePEG interfaces, and that "
     "this interface should be used with care.",
     &BoseEinsteinHandler::theAdditionalP8Settings, -1, "", "", "",
     true, false, Interface::nolimits);
#endif
#if PYTHIA_VERSION_INTEGER==8301
  static Switch<BoseEinsteinHandler,bool> interfaceBoseEinstein_Pion
    ("BoseEinstein_Pion",
     "  Include effects or not for identical \\f$pi^+\\f$, \\f$pi^-\\f$  and \\f$pi^0\\f$.  ",
     &BoseEinsteinHandler::theBoseEinstein_Pion, true, true, false);
  static SwitchOption interfaceBoseEinstein_PionOn
    (interfaceBoseEinstein_Pion,
     "On",
     "On",
     true);
  static SwitchOption interfaceBoseEinstein_PionOff
    (interfaceBoseEinstein_Pion,
     "Off",
     "Off",
     false);

  static Switch<BoseEinsteinHandler,bool> interfaceBoseEinstein_Kaon
    ("BoseEinstein_Kaon",
     "  Include effects or not for identical \\f$K^+\\f$, \\f$K^-\\f$,  \\f$K_S^0\\f$ and \\f$K_L^0\\f$.  ",
     &BoseEinsteinHandler::theBoseEinstein_Kaon, true, true, false);
  static SwitchOption interfaceBoseEinstein_KaonOn
    (interfaceBoseEinstein_Kaon,
     "On",
     "On",
     true);
  static SwitchOption interfaceBoseEinstein_KaonOff
    (interfaceBoseEinstein_Kaon,
     "Off",
     "Off",
     false);

  static Switch<BoseEinsteinHandler,bool> interfaceBoseEinstein_Eta
    ("BoseEinstein_Eta",
     "  Include effects or not for identical \\f$eta\\f$ and \\f$eta'\\f$.  ",
     &BoseEinsteinHandler::theBoseEinstein_Eta, true, true, false);
  static SwitchOption interfaceBoseEinstein_EtaOn
    (interfaceBoseEinstein_Eta,
     "On",
     "On",
     true);
  static SwitchOption interfaceBoseEinstein_EtaOff
    (interfaceBoseEinstein_Eta,
     "Off",
     "Off",
     false);

  static Parameter<BoseEinsteinHandler,double> interfaceBoseEinstein_lambda
    ("BoseEinstein_lambda",
     "  The strength parameter for Bose-Einstein effects. On physical grounds  it should not be above unity, but imperfections in the formalism  used may require that nevertheless.  ",
     &BoseEinsteinHandler::theBoseEinstein_lambda, 1., 0., 2.,
     true, false, Interface::limited);

  static Parameter<BoseEinsteinHandler,double> interfaceBoseEinstein_QRef
    ("BoseEinstein_QRef",
     "  The size parameter of the region in \\f$Q\\f$ space over which  Bose-Einstein effects are significant.  Can be thought of as  the inverse of an effective distance in normal space,  \\f$R = hbar / QRef\\f$, with \\f$R\\f$ as used in the above equation.  That is, \\f$f_2(Q) = (1 + lambda * exp(-(Q/QRef)^2)) * (...)\\f$.  ",
     &BoseEinsteinHandler::theBoseEinstein_QRef, 0.2, 0.05, 1.,
     true, false, Interface::limited);

  static Parameter<BoseEinsteinHandler,double> interfaceBoseEinstein_widthSep
    ("BoseEinstein_widthSep",
     "  Particle species with a width above this value (in GeV) are assumed  to be so short-lived that they decay before Bose-Einstein effects  are considered, while otherwise they do not. In the former case the  decay products thus can obtain shifted momenta, in the latter not.  The default has been picked such that both \\f$rho\\f$ and  \\f$K^*\\f$ decay products would be modified.  ",
     &BoseEinsteinHandler::theBoseEinstein_widthSep, 0.02, 0.001, 1.,
     true, false, Interface::limited);

  static ParVector<BoseEinsteinHandler,string> interfaceAdditionalP8Settings
    ("AdditionalP8Settings",
     "Each string in this vector will be sent to the Pythia8 Settings "
     "object using the Pythia::readString function. Note that most relevant "
     "settings are already available as normal ThePEG interfaces, and that "
     "this interface should be used with care.",
     &BoseEinsteinHandler::theAdditionalP8Settings, -1, "", "", "",
     true, false, Interface::nolimits);
#endif

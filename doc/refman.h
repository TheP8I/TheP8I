/**\mainpage TheP8I Reference Manual

This is the reference manual for THEP8I, the interface
of the PYTHIA8 event generator to the Toolkit for High
Energy Physics Event Generation - THEPEG.

\section Introduction Introduction

THEP8I wraps different parts of PYTHIA8
event generator into handler classes which can be used within
THEPEG.

*/
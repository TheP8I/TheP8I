# Introduction
The aim of the TheP8I  is to introduce the Lund string fragmentation 
model into the ThePEG Monte Carlo Event Generator toolkit and, 
therefore, to the Monte Carlo Event Generators (MCEGs) which use 
ThePEG, e.g. Herwig7 https://herwig.hepforge.org/, 
J.Bellm *et al.*, "Herwig 7.2 Release Note",  arXiv:1912.06509 [hep-ph].

The details on the application of the TheP8I can be found, for instance 
in

J.Bellm and L.Gellersen, "High dimensional parameter tuning for event 
generators"  Eur.Phys.J. **C**80 (2020) no.1, 54



# Authors
- Leif Lönnblad

- Johannes  Bellm

- Andrii Verbytskyi

with contributions from

- Christian Bierlich

- David Grellscheid

- Simon Plätzer




# Installation 
The prereqirements for the package are 

- C++11 compiller
- `Pythia8` with development headers
- `ThePEG` with  development headers
  To run the tests `ThePEG` should have `HepMC2` support and loadable `HepMC2`
  libraries.
- `Perl`
- `GSL` with development headers
- some binutils, e.g. `sed`

- Since version 2.0.2 TheP8I can be build also with `cmake>3.10`.


Optionally

 - `doxygen` and `latex` for the documentation generation
 - `Herwig` for the tests. In this case the environment to run the Herwig 
 should be functional, i.e. all the libraries required by the Herwig 
 should be available. Typically this means the libraries like boost, 
 HepMC, lhapdf, fastjet, EvtGen, Tauola and Photos should be in the 
 `LD_LIBRARY_PATH` and the variables like `LHAPDF_DATA_PATH` and 
 `PYTHIA8DATA` should be set correctly. Please see the configuration of 
 the continuos integration setup (.gitlab-ci.yml) for an example.

While most recent (as of 2022) versions of the ThePEG and Pythia8 should 
work, it is recommended to use the ThePEG>=2.1.4 and Pythia8>=8.230.

The build system of the TheP8I package is based on autotools. The 
configuration is done via the `configure` script, e.g.

``
./configure --prefix=/home/user/mythep8i --with-pythia8=/usr
``

Please see the full list of the available options with 

``
./configure --help
``

After the configuration, the compilation and installation can be done 
with the 

``
make
``
and

``
make install
``
The latest command will install the compiled library and documentation 
in the specified prefix.

The doxygen documentation can be build with 

``
make -C Doc refman-html
``

The tests can be executed with 

``
make check
``

The latest command  will run some event generation with an output in 
HepMC files using the compiled TheP8I library. By default only tests 
with the ThePEG simple event generator will be executed.  In case the 
`--with-herwig` option is specified some tests/examples with the TheP8I 
inside Herwig will be executed.

Please note that the TheP8I configuration scripts generate the interface 
using the actual Pythia8 installation  using the description of classes 
and parameters in the xml files in the PYTHIA8_XMLDIR. Therefore, 
(e.g. in case the xml files were corrupted) the interfaces migh differ 
for different Pythia8 installations. 
If there are problems with the generation of the interfaces to the existing 
Pythia8 installation, TheP8I can be configured to use the pregenerated 
interfaces, which are available for a limited number of Pythia8 versions.
To use them add `--enable-interface-generation=no` to the configure options.

To build TheP8I with `cmake` and full control over the location of dependencies
``
cmake -S.  -B BUILD -DCMAKE_INSTALL_PREFIX=/where/to/install -DTHEP8I_GENERATE_INTERFACES=ON -DTHEP8I_ENABLE_TEST=ON  -DTHEP8I_BUILD_DOCS=ON -DPYTHI8_ROOT_DIR=/where/is/pythia -DTHEPEG_ROOT_DIR=/where/is/thepeg -DHERWIG_ROOT_DIR=/where/is/herwig -DGSL_ROOT_DIR=/where/is/gsl
``

To do the compilation, installation and perform some tests:
``
cmake --build BUILD
cmake -- install BUILD
cd BUILD
ctest . --verbose
``
In the future versions of TheP8I, cmake will be the recommended build system.


# Usage 

To use the TheP8I for an event generation inside Herwig only the library 
itself is needed. The examples of Herwig config files are given in the 
test directory. The location of the TheP8I library should be visible 
for Herwig, e.g. the Herwig should be called with -l flag:

``
Herwig read MyConfig.in   -l/directory/with/TheP8I/library
``

``
Herwig run MyConfig.run   -l/directory/with/TheP8I/library
``

For the correct event generation, it is highly recommended that all the 
particle properties will be kept consistent in Pythia8, 
ThePEG/Herwig and other used packages. This can be archieved with 
setting the properties of particles in ThePEG and/or Pythia8 explicitely
via the appropriate interfaces. 
Alternatively, the automatically generated list of Pythia8 particle 
properties in ThePEG format is created during the compilation of TheP8I.
This list of particle properties, `TheP8Particles.in`,  can be loaded in 
the Herwig to replace the native one and, therefore reach consistency 
between the Pythia8 and Herwig particle data.
Please note that for multiple decays in the `TheP8IParticles.in` the
`/TheP8I/Decayers/FlatDecayer` interface is used.

Please note that the default string fragmentation parameters when using
TheP8I are the same as in Pythia8, and they are tuned together with the
default final state shower in Pythia8. If used together with Herwig7 it
is strongly recommended to first retune these parameter with the Herwig
parton shower.


 

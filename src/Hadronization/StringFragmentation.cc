//
// This is the implementation of the non-inlined, non-templated member
// functions of the StringFragmentation class.
//

#include "StringFragmentation.h"
#include "Ropewalk.h"
#include "RandomAverageHandler.h"
#include "RandomHandler.h"
#include "ThePEG/Interface/ClassDocumentation.h"
#include "ThePEG/Interface/Parameter.h"
#include "ThePEG/Interface/Switch.h"
#include "ThePEG/Interface/ParVector.h"
#include "ThePEG/Interface/Reference.h"
#include "ThePEG/PDT/RemnantDecayer.h"
#include "ThePEG/EventRecord/Particle.h"
#include "ThePEG/EventRecord/Step.h"
#include "ThePEG/Handlers/EventHandler.h"
#include "ThePEG/Repository/EventGenerator.h"
#include "ThePEG/Utilities/DebugItem.h"
#include "ThePEG/Utilities/EnumIO.h"
#include "ThePEG/Utilities/MaxCmp.h"
#include "ThePEG/Utilities/HoldFlag.h"
#include "ThePEG/Persistency/PersistentOStream.h"
#include "ThePEG/Persistency/PersistentIStream.h"
#include <algorithm>
#include <cmath>
using namespace TheP8I;


StringFragmentation::StringFragmentation()
    :  pythia(), fScheme(0), stringR0(0.5*femtometer), stringm0(1.0*GeV), junctionDiquark(1.0), alpha(1.0), average(true),
       stringyCut(2.0), stringpTCut(6*GeV), fragmentationMass(0.134),
       baryonSuppression(0.5), window(false), minStringy(8.0), longSoft(false), longStringsOnly(false),
       throwaway(false), shoving(false), deltay(5.0), tshove(1.0*femtometer),
       deltat(0.1*femtometer), rCutOff(4.0), gAmplitude(1.0*GeV/femtometer), gExponent(1.0), yCutOff(4.0), lorentz(true), cylinder(false),
       opHandler(0), maxTries(2),
#include "StringFragmentation-init.h"
{
}

StringFragmentation::~StringFragmentation() {

    if ( opHandler ) delete opHandler;

}

void StringFragmentation::handle(EventHandler & eh, const tPVector & tagged,
                                 const Hint & h) {
    ++nev;
    static unsigned long lastEventId = 0;
    bool secondary = false;
    // Get the event
    tPVector all = RemnantDecayer::decayRemnants(tagged, *newStep());
    HoldFlag<int> oldFS(fScheme, fScheme);
    // Prevent funny behavior if hadronizing decays of eg. Upsilon.
    if ( newStep()->collision()->uniqueId == lastEventId ) {
        secondary = true;
        if(fScheme == 4 || fScheme == 5 || fScheme == 1) fScheme = 99;
        else fScheme = 0;
    }
    else
        lastEventId = newStep()->collision()->uniqueId;

    std::vector<ColourSinglet> singlets;
    singlets.clear();

    if ( theCollapser && !secondary )
        singlets =  theCollapser->collapse(all, newStep());
    else
        singlets = ColourSinglet::getSinglets(all.begin(), all.end());
    // Goto correct hadronization scheme
    switch(fScheme) {
    case 0: // Pythia
    {
        hadronizeSystems(*opHandler->GetPythiaPtr(1.0,nev%10000==0), singlets, all);
        break;
    }
    case 1: // For playing around and printing stuff
    {
        // Cartoon of string shoving
        //
        if(singlets.size() > 10) {
            Ropewalk ropewalkInit(singlets, stringR0, stringm0,
                                  baryonSuppression, throwaway, false);
            std::vector< std::pair<ColourSinglet,double> > allStrings =
                ropewalkInit.getSinglets(stringyCut);
            tPVector allParticles;
            for ( std::vector< std::pair<ColourSinglet,double> >::iterator sItr = allStrings.begin();
                    sItr != allStrings.end(); ++sItr )
                for( tcPVector::iterator pItr = sItr->first.partons().begin();
                        pItr != sItr->first.partons().end(); ++pItr ) {
                    allParticles.push_back(const_ptr_cast<tPPtr>(*pItr));

                }
            singlets =  theCollapser->collapse(allParticles, newStep());
            for(double time = 0.0; time < 1.0; time += 0.1) {
                Ropewalk ropewalkOne(singlets, stringR0, stringm0,
                                     baryonSuppression, false, shoving, rCutOff, gAmplitude, gExponent,
                                     yCutOff, deltay, time*femtometer, 0.1*femtometer, lorentz, cylinder, stringpTCut, minStringy, longSoft,  false);
                auto css = ropewalkOne.getSinglets(1.0);
                std::vector<ColourSinglet> cs;
                for(auto itr = css.begin(); itr != css.end(); ++itr)
                    cs.push_back(itr->first);



            }
            exit(1);
        }

        break;

    }
    case 2: // Do  shoving and construct parton level histograms.
    {




        if (throwaway) {
            Ropewalk ropewalkInit(singlets, stringR0, stringm0, baryonSuppression, throwaway, false);

            std::vector< std::pair<ColourSinglet,double> > allStrings = ropewalkInit.getSinglets(stringyCut);
            tPVector allParticles;
            for ( std::vector< std::pair<ColourSinglet,double> >::iterator sItr = allStrings.begin(); sItr != allStrings.end(); ++sItr )
                for( tcPVector::iterator pItr = sItr->first.partons().begin(); pItr != sItr->first.partons().end(); ++pItr ) {
                    allParticles.push_back(const_ptr_cast<tPPtr>(*pItr));
                }
            singlets =  theCollapser->collapse(allParticles, newStep());
        }


        typedef std::multimap<Energy,Ropewalk::DipoleMap::const_iterator> OrderedMap;
        OrderedMap ordered;
        Ropewalk ropewalk(singlets, stringR0, stringm0,
                          baryonSuppression, false, shoving, rCutOff, gAmplitude, gExponent,
                          yCutOff, deltay, tshove, deltat, lorentz, cylinder, stringpTCut, minStringy, longSoft,  false);

        for(Ropewalk::DipoleMap::iterator itr = ropewalk.begin(); itr != ropewalk.end(); ++itr)
            ordered.insert(std::make_pair(maxPT(*itr->first, stringyCut), itr));

        for(OrderedMap::iterator it = ordered.begin(); it != ordered.end(); ++it ) {

            if(!pythia.getRopeUserHooksPtr()->setDipoles(&(*it->second), stringm0, stringR0, !average, alpha)) {
                pythia.getRopeUserHooksPtr()->setEnhancement(ropewalk.averageKappaEnhancement(it->second, stringyCut));
                for (int i = 0, N = it->second->second.size(); i < N; ++i)
                    it->second->second[i]->hadr = true;
            }
            std::vector<ColourSinglet> toHadronization(1, *(it->second->first));
            forcerun = false;
            if(!hadronizeSystems(pythia, toHadronization, all))
                hadronizeSystems(pythia, toHadronization, all);
        }

        break;
    }
    case 3: // Pipes with average
    {
        RandomAverageHandler avg_enhancer(throwaway);
        // TODO: Implement throwaway functionality in this
        avg_enhancer.clear();
        std::vector<StringPipe> pipes;
        // Make all the pipes
        for(std::vector<ColourSinglet>::iterator sItr = singlets.begin(); sItr!=singlets.end(); ++sItr)
            pipes.push_back(StringPipe(&(*sItr),stringR0));

        avg_enhancer.SetEvent(pipes);
        for(std::vector<StringPipe>::iterator it = pipes.begin(); it!=pipes.end(); ++it) {
            std::vector<ColourSinglet> toHadronization;
            double h_ = avg_enhancer.KappaEnhancement(*it);
            if(h_ > 0) {
                toHadronization.push_back(*(*it).GetSingletPtr());
                forcerun = false;
                if(!hadronizeSystems(*(opHandler->GetPythiaPtr(h_,nev%10000==0)),toHadronization,all))
                    hadronizeSystems(*(opHandler->GetPythiaPtr(1.0,false)),toHadronization,all);
            }
        }
        break;
    }
    case 4: // Average kappa over whole strings
    {
        Ropewalk ropewalk(singlets, stringR0, stringm0, baryonSuppression,throwaway, false);
        std::vector< std::pair<ColourSinglet,double> > strings = ropewalk.getSinglets(stringyCut);
        std::vector<ColourSinglet> toHadronization;
        double avge = 0;
        for(int i = 0, N = strings.size(); i < N; ++i ) {
            avge += strings[i].second;
            pythia.getRopeUserHooksPtr()->setEnhancement(strings[i].second);
            toHadronization.clear();
            toHadronization.push_back(strings[i].first);
            hadronizeSystems(pythia,toHadronization,all);
        }
        break;
    }
    case 5: // Altering kappa per dipole
    {


        if (throwaway) {
            Ropewalk ropewalkInit(singlets, stringR0, stringm0, baryonSuppression, throwaway, false);

            std::vector< std::pair<ColourSinglet,double> > allStrings = ropewalkInit.getSinglets(stringyCut);
            tPVector allParticles;
            for ( std::vector< std::pair<ColourSinglet,double> >::iterator sItr = allStrings.begin(); sItr != allStrings.end(); ++sItr )
                for( tcPVector::iterator pItr = sItr->first.partons().begin(); pItr != sItr->first.partons().end(); ++pItr ) {
                    allParticles.push_back(const_ptr_cast<tPPtr>(*pItr));
                }
            singlets =  theCollapser->collapse(allParticles, newStep());
        }


        typedef std::multimap<Energy,Ropewalk::DipoleMap::const_iterator> OrderedMap;
        OrderedMap ordered;
        Ropewalk ropewalk(singlets, stringR0, stringm0,
                          baryonSuppression, false, shoving, rCutOff, gAmplitude, gExponent,
                          yCutOff, deltay, tshove, deltat, lorentz, cylinder, stringpTCut,
                          minStringy, longSoft, false);

        for(Ropewalk::DipoleMap::iterator itr = ropewalk.begin(); itr != ropewalk.end(); ++itr)
            ordered.insert(std::make_pair(maxPT(*itr->first, stringyCut), itr));

        for(OrderedMap::iterator it = ordered.begin(); it != ordered.end(); ++it ) {

            if(!pythia.getRopeUserHooksPtr()->setDipoles(&(*it->second), stringm0, stringR0, !average, alpha)) {
                pythia.getRopeUserHooksPtr()->setEnhancement(ropewalk.averageKappaEnhancement(it->second, stringyCut));
                for (int i = 0, N = it->second->second.size(); i < N; ++i)
                    it->second->second[i]->hadr = true;
            }
            std::vector<ColourSinglet> toHadronization(1, *(it->second->first));
            forcerun = false;
            if(!hadronizeSystems(pythia, toHadronization, all))
                hadronizeSystems(pythia, toHadronization, all);
        }
        if(!longStringsOnly) {
            ordered.clear();
            for(Ropewalk::DipoleMap::iterator itr = ropewalk.beginSpectators(); itr != ropewalk.endSpectators(); ++itr)
                ordered.insert(std::make_pair(maxPT(*itr->first, stringyCut), itr));
            for(OrderedMap::iterator it = ordered.begin(); it != ordered.end(); ++it ) {

                pythia.getRopeUserHooksPtr()->setEnhancement(1.0);
                std::vector<ColourSinglet> toHadronization(1, *(it->second->first));
                forcerun = false;
                if(!hadronizeSystems(pythia, toHadronization, all))
                    hadronizeSystems(pythia, toHadronization, all);
            }
        }
        break;
    }
    case 99: // New Pythia
    {
        pythia.getRopeUserHooksPtr()->setEnhancement(1.0);
        hadronizeSystems(pythia, singlets, all);
        break;
    }
    default:
        std::cout << "We should really not be here. This is bad..." << std::endl;
    }

    //      fScheme = oldFS;

}

void StringFragmentation::dofinish() {


}

Energy StringFragmentation::maxPT(const ColourSinglet & cs, double deltaY) {
    MaxCmp<Energy2> maxpt2;
    for ( int i = 0, N = cs.partons().size(); i < N; ++i ) {
        if ( deltaY > 0.0 && abs(cs.partons()[i]->eta()) > deltaY ) continue;
        maxpt2(cs.partons()[i]->momentum().perp2());
    }
    if ( !maxpt2 ) return ZERO;
    return ThePEG::sqrt(maxpt2.value());
}

bool StringFragmentation::
hadronizeSystems(Pythia8Interface & pyt, const std::vector<ColourSinglet> & singlets, const tPVector & all) {
    Pythia8::Event & event = pyt.event();
    pyt.clearEvent();

    for ( int i = 0, N = singlets.size(); i < N; ++i ) {

        if ( singlets[i].nPieces() == 3 ) {
            // Simple junction.
            // Save place where we will store dummy particle.
            int nsave = event.size();
            event.append(22, -21, 0, 0, 0, 0, 0, 0, 0.0, 0.0, 0.0, 0.0);
            int npart = 0;
            for ( int ip = 1; ip <= 3; ++ip )
                for ( int j = 0, M = singlets[i].piece(ip).size(); j < M; ++j ) {
                    pyt.addParticle(singlets[i].piece(ip)[j], 23, nsave, 0);
                    ++npart;
                }
            event[nsave].daughters(nsave + 1, nsave + npart);
        }
        else if ( singlets[i].nPieces() == 5 ) {
            // Double, connected junction
            // Save place where we will store dummy beam particles.
            int nb1 = event.size();
            int nb2 = nb1 + 1;
            event.append(2212, -21, 0, 0, nb1 + 2, nb1 + 4, 0, 0,
                         0.0, 0.0, 0.0, 0.0);
            event.append(-2212, -21, 0, 0, nb2 + 4, nb2 + 6, 0, 0,
                         0.0, 0.0, 0.0, 0.0);

            // Find the string piece connecting the junctions, and the
            // other loose string pieces.
            int connector = 0;
            int q1 = 0;
            int q2 = 0;
            int aq1 = 0;
            int aq2 = 0;
            for ( int ip = 1; ip <= 5; ++ip ) {
                if ( singlets[i].sink(ip).first && singlets[i].source(ip).first )
                    connector = ip;
                else if ( singlets[i].source(ip).first ) {
                    if ( q1 ) q2 = ip;
                    else q1 = ip;
                }
                else if ( singlets[i].sink(ip).first ) {
                    if ( aq1 ) aq2 = ip;
                    else aq1 = ip;
                }
            }
            if ( !connector || !q1 || !q2 || !aq1 || ! aq2 )
                Throw<StringFragError>()
                        << name() << " found complicated junction string. Although Pythia8 can "
                        << "hadronize junction strings, this one was too complicated."
                        << Exception::runerror;

            // Insert the partons of the loose triplet ends.
            int start = event.size();
            for ( int j = 0, M = singlets[i].piece(q1).size(); j < M; ++j )
                pyt.addParticle(singlets[i].piece(q1)[j], 23, nb1, 0);
            for ( int j = 0, M = singlets[i].piece(q2).size(); j < M; ++j )
                pyt.addParticle(singlets[i].piece(q2)[j], 23, nb1, 0);
            // Insert dummy triplet incoming parton with correct colour code.
            int col1 = singlets[i].piece(connector).empty()? event.nextColTag():
                       pyt.addColourLine(singlets[i].piece(connector).front()->colourLine());
            int dum1 = event.size();
            event.append(2, -21, nb1, 0, 0, 0, col1, 0, 0.0, 0.0, 0.0, 0.0, 0.0);
            event[nb1].daughters(start, start + singlets[i].piece(q1).size() +
                                 singlets[i].piece(q2).size());

            // Insert the partons of the loose anti-triplet ends.
            start = event.size();
            for ( int j = 0, M = singlets[i].piece(aq1).size(); j < M; ++j )
                pyt.addParticle(singlets[i].piece(aq1)[j], 23, nb2, 0);
            for ( int j = 0, M = singlets[i].piece(aq2).size(); j < M; ++j )
                pyt.addParticle(singlets[i].piece(aq2)[j], 23, nb2, 0);
            // Insert dummy anti-triplet incoming parton with correct colour code.
            int col2 = singlets[i].piece(connector).empty()? col1:
                       pyt.addColourLine(singlets[i].piece(connector).back()->antiColourLine());
            int dum2 = event.size();
            event.append(-2, -21, nb2, 0, 0, 0, 0, col2, 0.0, 0.0, 0.0, 0.0, 0.0);
            event[nb2].daughters(start, start + singlets[i].piece(aq1).size() +
                                 singlets[i].piece(aq2).size());

            // Add the partons from the connecting string piece.
            for ( int j = 0, M = singlets[i].piece(connector).size(); j < M; ++j )
                pyt.addParticle(singlets[i].piece(connector)[j], 23, dum1, dum2);
        }
        else if ( singlets[i].nPieces() > 1 ) {
            // We don't know how to handle other junctions yet.
            Throw<StringFragError>()
                    << name() << " found complicated junction string. Although Pythia8 can "
                    << "hadronize junction strings, that interface is not ready yet."
                    << Exception::runerror;
        } else {
            // Normal string
            for ( int j = 0, M = singlets[i].partons().size(); j < M; ++j )
                pyt.addParticle(singlets[i].partons()[j], 23, 0, 0);
        }

    }

    for ( int i = 0, N = all.size(); i < N; ++i )
        if ( !all[i]->coloured() )
            pyt.addParticle(all[i], 23, 0, 0);

    int oldsize = event.size();

    Pythia8::Event saveEvent = event;
    int ntry = maxTries;
    CurrentGenerator::Redirect stdout(cout, false);
    while ( !pyt.go() && --ntry ) event = saveEvent;

    //event.list(cout);
    //event.list(cerr);
    if ( !ntry ) {
        static DebugItem printfailed("TheP8I::PrintFailed", 10);
        if ( printfailed ) {
            double ymax = -1000.0;
            double ymin = 1000.0;
            double sumdy = 0.0;
            for ( int i = 0, N = singlets.size(); i < N; ++i )
                for ( int j = 0, M = singlets[i].partons().size(); j < M; ++j ) {
                    const Particle & p = *singlets[i].partons()[j];
                    /**  This code calculates rapidity as is */
                    ymax = std::max(ymax, p.momentum().rapidity());
                    ymin = std::min(ymin, p.momentum().rapidity());
                    /**  This code calculates rapidity using thrust
                    ymax = std::max(ymax, (_es ? _es->yT(p.momentum()) : p.momentum().rapidity()));
                    ymin = std::min(ymin,(_es ? _es->yT(p.momentum()) : p.momentum().rapidity()));
                    */

                    std::cerr << std::setw(5) << j << std::setw(14) << p.momentum().rapidity()
                              << std::setw(14) << p.momentum().perp()/GeV
                              << std::setw(14) << p.momentum().m()/GeV;
                    if ( j == 0 && p.data().iColour() == PDT::Colour8 ) {
                        std::cerr << std::setw(14) << (p.momentum() + singlets[i].partons().back()->momentum()).m()/GeV;
                        /** Calculation of rapidity with thrust
                        sumdy += std::abs((_es ? _es->yT(p.momentum()) : p.momentum().rapidity()) ) -(_es ? _es->yT(singlets[i].partons().back()->momentum())
                                 : singlets[i].partons().back()->momentum().rapidity());
                         */
                        /** Calculation of rapidity as is */
                        sumdy += std::abs(p.momentum().rapidity() - singlets[i].partons().back()->momentum().rapidity());
                    }
                    else if ( j > 0 ) {
                        std::cerr << std::setw(14) << (p.momentum() + singlets[i].partons()[j-1]->momentum()).m()/GeV;
                        /** Calculation of rapidity with thrust
                        sumdy += std::abs((_es ? _es->yT(p.momentum()) : p.momentum().rapidity()) ) -(_es ? _es->yT(singlets[i].partons()[j-i]->momentum())
                                 : singlets[i].partons()[j-i]->momentum().rapidity());
                         */
                        /** Calculation of rapidity as is */
                        sumdy += std::abs(p.momentum().rapidity() - singlets[i].partons()[j-1]->momentum().rapidity());
                        if ( j + 1 < M )
                            std::cerr << std::setw(14) << (p.momentum() + singlets[i].partons()[j-1]->momentum()).m()*
                                      (p.momentum() + singlets[i].partons()[j+1]->momentum()).m()/
                                      (p.momentum() + singlets[i].partons()[j+1]->momentum() +
                                       singlets[i].partons()[j-1]->momentum()).m()/GeV;
                    }
                    std::cerr << std::endl;
                }
            std::cerr << std::setw(14) << ymax - ymin << std::setw(14) << sumdy/(ymax - ymin) << std::endl;
        }
        CurrentGenerator::Redirect stdout2(cout, true);
        //event.list();
        pyt.errorlist();
        std::cout << "ThePEG event listing:\n" << *(generator()->currentEvent());
        Throw<StringFragError>()
                << "Pythia8 failed to hadronize partonic state:\n"
                << stdout2.str() << "This event will be discarded!\n"
                << Exception::warning;
        throw Veto();
    }
    if(window) {
        //	cout << stringyCut << " " << stringpTCut/GeV << endl;
        if(forcerun) forcerun = false;
        else {
            for( int i = 1, N = event.size(); i < N; ++i ) {
                tPPtr p = pyt.getParticle(i);
                //  std::cout << p->momentum().perp()/GeV << " " << p->rapidity() << " " << p->id() << std::endl;
                if (p->momentum().perp() > stringpTCut && abs(p->rapidity()) < stringyCut ) {
                    /* if(std::abs(p->id()) == 310 || std::abs(p->id()) == 3122){
                       static ofstream fout(( generator()->runName() + ".txt").c_str(),ios::app);
                       tcPVector pVector = singlets[0].partons();
                       fout << p->id() << " " << generator()->currentEvent()->weight() << " ";
                       for(size_t j=0;j<pVector.size();++j){
                         //if(pVector[j]->id()<20){
                           fout << pVector[j]->id() << " " << pVector[j]->momentum().perp()/GeV;
                           fout.close();
                         //}
                       }
                       */

                    forcerun = true;
                    return false;
                }
            }
        }
    }

    std::map<tPPtr, std::set<tPPtr> > children;
    std::map<tPPtr, std::set<tPPtr> > parents;
    for ( int i = 1, N = event.size(); i < N; ++i ) {
        tPPtr p = pyt.getParticle(i);
        int d1 = event[i].daughter1();
        if ( d1 <= 0 ) continue;
        children[p].insert(pyt.getParticle(d1));
        parents[pyt.getParticle(d1)].insert(p);
        int d2 = event[i].daughter2();
        if ( d2 > 0 ) {
            children[p].insert(pyt.getParticle(d2));
            parents[pyt.getParticle(d2)].insert(p);
        }
        for ( int di = d1 + 1; di < d2; ++di ) {
            children[p].insert(pyt.getParticle(di));
            parents[pyt.getParticle(di)].insert(p);
        }
    }

    for ( int i = oldsize, N = event.size(); i < N; ++i ) {
        PPtr p = pyt.getParticle(i);
        std::set<tPPtr> & pars = parents[p];
        if ( !p ) {
            event.list();
            std::cout << i << std::endl;
            Throw<StringFragError>()
                    << "Failed to reconstruct hadronized state from Pythia8:\n"
                    << stdout.str() << "This event will be discarded!\n" << Exception::warning;
            throw Veto();
        }
        double temp_ifnan=p->momentum().perp()/GeV; //OSX
        if ( std::isnan((temp_ifnan)) ) {
            Throw<StringFragError>()
                    << "Failed to reconstruct hadronized state from Pythia8:\n"
                    << stdout.str() << "This event will be discarded!\n" << Exception::warning;
            throw Veto();
        }
        if ( pars.empty() ) {
            Pythia8::Particle & pyp = event[i];
            if ( pyp.mother1() > 0 ) pars.insert(pyt.getParticle(pyp.mother1()));
            if ( pyp.mother2() > 0 ) pars.insert(pyt.getParticle(pyp.mother2()));
            for ( int im = pyp.mother1() + 1; im < pyp.mother2(); ++im )
                pars.insert(pyt.getParticle(im));
            if ( pars.empty() ) {
                Throw<StringFragError>()
                        << "Failed to reconstruct hadronized state from Pythia8:\n"
                        << stdout.str() << "This event will be discarded!\n" << Exception::warning;
                throw Veto();
            }
        }
        if ( pars.size() == 1 ) {
            tPPtr par = *pars.begin();
            if ( children[par].size() == 1 &&
                    *children[par].begin() == p && par->id() == p->id() )
                newStep()->setCopy(par, p);
            else
                newStep()->addDecayProduct(par, p);
        } else {
            newStep()->addDecayProduct(pars.begin(), pars.end(), p);
        }
    }
    return true;
}

std::string StringFragmentation::PrintStringsToMatlab(std::vector<ColourSinglet>& singlets) {
    std::stringstream ss;
    std::vector<std::vector<double> > drawit;
    std::vector<char> names;
    for(int i=0; i<26; ++i) names.push_back(char(i+97));
    std::vector<char>::iterator nItr = names.begin();


    for(std::vector<ColourSinglet>::iterator sItr = singlets.begin(); sItr!=singlets.end(); ++sItr) {
        drawit.clear();
        for (tcPVector::iterator pItr = sItr->partons().begin(); pItr!=sItr->partons().end(); ++pItr) {
            std::vector<double> tmp;
            tmp.clear();
            tmp.push_back((*pItr)->rapidity());
            tmp.push_back((*pItr)->vertex().x()/femtometer);
            tmp.push_back((*pItr)->vertex().y()/femtometer);
            drawit.push_back(tmp);
        }
        ss << *nItr << " = [";
        for(int i=0, N=drawit.size(); i<N; ++i) {
            ss << drawit[i].at(0) << ", " << drawit[i].at(1) << ", " << drawit[i].at(2) << ";" << std::endl;
        }
        ss << "]';\n\n" << std::endl;
        ++nItr;
    }
    return ss.str();
}


IBPtr StringFragmentation::clone() const {
    return new_ptr(*this);
}

IBPtr StringFragmentation::fullclone() const {
    return new_ptr(*this);
}


void StringFragmentation::doinitrun() {
    HadronizationHandler::doinitrun();

    theAdditionalP8Settings.push_back("ProcessLevel:all = off");
    theAdditionalP8Settings.push_back("HadronLevel:Decay = off");
    theAdditionalP8Settings.push_back("Check:event = off");
    theAdditionalP8Settings.push_back("Next:numberCount = 0");
    theAdditionalP8Settings.push_back("Next:numberShowLHA = 0");
    theAdditionalP8Settings.push_back("Next:numberShowInfo = 0");
    theAdditionalP8Settings.push_back("Next:numberShowProcess = 0");
    theAdditionalP8Settings.push_back("Next:numberShowEvent = 0");
    theAdditionalP8Settings.push_back("Init:showChangedSettings = 0");
    theAdditionalP8Settings.push_back("Init:showAllSettings = 0");
    theAdditionalP8Settings.push_back("Init:showChangedParticleData = 0");
    theAdditionalP8Settings.push_back("Init:showChangedResonanceData = 0");
    theAdditionalP8Settings.push_back("Init:showAllParticleData = 0");
    theAdditionalP8Settings.push_back("Init:showOneParticleData = 0");
    theAdditionalP8Settings.push_back("Init:showProcesses = 0");

    if(fScheme == 4 || fScheme == 5 || fScheme == 1) { // We don't need multiple Pythia objects anymore!
        pythia.enableHooks();
        pythia.init(*this,theAdditionalP8Settings);

        PytPars p;

        p.insert(std::make_pair("StringPT:sigma",theStringPT_sigma));
        p.insert(std::make_pair("StringZ:aLund",theStringZ_aLund));
        p.insert(std::make_pair("StringZ:bLund",theStringZ_bLund));
        p.insert(std::make_pair("StringFlav:probStoUD",theStringFlav_probStoUD));
        p.insert(std::make_pair("StringFlav:probSQtoQQ",theStringFlav_probSQtoQQ));
        p.insert(std::make_pair("StringFlav:probQQ1toQQ0",theStringFlav_probQQ1toQQ0));
        p.insert(std::make_pair("StringFlav:probQQtoQ",theStringFlav_probQQtoQ));

        phandler.init(fragmentationMass*fragmentationMass,baryonSuppression,p);
        pythia.getRopeUserHooksPtr()->setParameterHandler(&phandler);

    }
    if( !( fScheme == 4  || fScheme == 5 || fScheme == 1) ) { // Not 'else' as it can change above...

        std::vector<std::string> moresettings = theAdditionalP8Settings;
        moresettings.push_back("StringPT:sigma = " + convert(theStringPT_sigma));
        moresettings.push_back("StringZ:aLund = " + convert(theStringZ_aLund));
        moresettings.push_back("StringZ:bLund = " + convert(theStringZ_bLund));
        moresettings.push_back("StringFlav:probStoUD = " +
                               convert(theStringFlav_probStoUD));
        moresettings.push_back("StringFlav:probSQtoQQ = " +
                               convert(theStringFlav_probSQtoQQ));
        moresettings.push_back("StringFlav:probQQ1toQQ0 = " +
                               convert(theStringFlav_probQQ1toQQ0));
        moresettings.push_back("StringFlav:probQQtoQ = " +
                               convert(theStringFlav_probQQtoQ));
        moresettings.push_back("OverlapStrings:fragMass = " +
                               convert(fragmentationMass));
        moresettings.push_back("OverlapStrings:baryonSuppression = " + convert(baryonSuppression));

        // Initialize the OverlapPythia Handler
        if ( opHandler ) delete opHandler;
        opHandler = new OverlapPythiaHandler(this,moresettings);

    }

    nev = 0;
}

void StringFragmentation::persistentOutput(PersistentOStream & os) const {
    os
#include "StringFragmentation-output.h"
            << fScheme << ounit(stringR0,femtometer) << ounit(stringm0,GeV) << junctionDiquark << alpha << average << stringyCut
            << ounit(stringpTCut,GeV) << fragmentationMass << baryonSuppression
            << oenum(window) << minStringy  << oenum(longSoft) << oenum(longStringsOnly) << oenum(throwaway) << oenum(shoving) << deltay << ounit(tshove,femtometer) << ounit(deltat,femtometer)
            << rCutOff << ounit(gAmplitude,GeV/femtometer) << gExponent << yCutOff << oenum(lorentz) << oenum(cylinder) <<  maxTries
            << theCollapser;
}

void StringFragmentation::persistentInput(PersistentIStream & is, int) {
    is
#include "StringFragmentation-input.h"
            >> fScheme >> iunit(stringR0,femtometer) >> iunit(stringm0,GeV) >> junctionDiquark >> alpha >> average >> stringyCut
            >> iunit(stringpTCut,GeV) >> fragmentationMass >> baryonSuppression
            >> ienum(window) >> minStringy >> ienum(longSoft) >> ienum(longStringsOnly) >> ienum(throwaway) >> ienum(shoving) >> deltay >> iunit(tshove,femtometer) >> iunit(deltat,femtometer)
            >> rCutOff >> iunit(gAmplitude,GeV/femtometer) >> gExponent >> yCutOff >> ienum(lorentz) >> ienum(cylinder) >>  maxTries
            >> theCollapser;
}

ClassDescription<StringFragmentation> StringFragmentation::initStringFragmentation;
// Definition of the static class description member.

void StringFragmentation::Init() {
#include "StringFragmentation-interfaces.h"
    static Reference<StringFragmentation,ClusterCollapser> interfaceCollapser
    ("Collapser",
     "A ThePEG::ClusterCollapser object used to collapse colour singlet "
     "clusters which are too small to fragment. If no object is given the "
     "MinistringFragmentetion of Pythia8 is used instead.",
     &StringFragmentation::theCollapser, true, false, true, true, false);

    static Switch<StringFragmentation,int> interfaceFragmentationScheme
    ("FragmentationScheme",
     "Different options for how to handle overlapping strings.",
     &StringFragmentation::fScheme, 0, true, false);
    static SwitchOption interfaceFragmentationSchemepythia
    (interfaceFragmentationScheme,
     "pythia",
     "Plain old Pythia fragmentation",
     0);
    static SwitchOption interfaceFragmentationSchemedep1
    (interfaceFragmentationScheme,
     "dep1",
     "Not sure about this.",
     1);
    static SwitchOption interfaceFragmentationSchemedep2
    (interfaceFragmentationScheme,
     "dep2",
     "Not sure about this.",
     2);
    static SwitchOption interfaceFragmentationSchemenone
    (interfaceFragmentationScheme,
     "none",
     "Plain old Pythia fragmentation",
     0);
    static SwitchOption interfaceFragmentationSchemepipe
    (interfaceFragmentationScheme,
     "pipe",
     "Each string is enclosed by a cylinder. Effective parameters are calculated from the overlap of cylinders.",
     3);
    static SwitchOption interfaceFragmentationSchemeaverage
    (interfaceFragmentationScheme,
     "average",
     "The overlap is calculated for each dipole in all strings, and for each string the effective parameters are obtained from the average overlap.",
     4);
    static SwitchOption interfaceFragmentationSchemedipole
    (interfaceFragmentationScheme,
     "dipole",
     "Effective parameters are calculated for each breakup by determining the overlap of the corresponding dipole with other dipoles.",
     5);


    static Switch<StringFragmentation,bool> interfaceThrowAway
    ("ThrowAway",
     "Throw away strings with weight:"
     "Use 1 - (p + q)/(m + n) for (0,-1) configuration.",
     &StringFragmentation::throwaway, false, true, false);
    static SwitchOption interfaceThrowAwayTrue
    (interfaceThrowAway,"True","enabled.",true);
    static SwitchOption interfaceThrowAwayFalse
    (interfaceThrowAway,"False","disabled.",false);

    static Switch<StringFragmentation,bool> interfaceShoving
    ("Shoving",
     "Strings will shove each other around.",
     &StringFragmentation::shoving, false, true, false);
    static SwitchOption interfaceShovingTrue
    (interfaceShoving,"True","enabled.",true);
    static SwitchOption interfaceShovingFalse
    (interfaceShoving,"False","disabled.",false);

    static Parameter<StringFragmentation,double> interfaceDeltaY
    ("DeltaY",
     "The size of rapidity slicing intervals",
     &StringFragmentation::deltay, 0, 0.1,
     0.0, 0,
     true, false, Interface::lowerlim);

    static Parameter<StringFragmentation,Length> interfaceTShove
    ("TShove",
     "The total shoving -and propagation time",
     &StringFragmentation::tshove, femtometer, 1.0*femtometer,
     0.0*femtometer, 0*femtometer,
     true, false, Interface::lowerlim);

    static Parameter<StringFragmentation,Length> interfaceDeltaT
    ("DeltaT",
     "Shoving time slicing -- size of a slice",
     &StringFragmentation::deltat, femtometer, 0.1*femtometer,
     0.0*femtometer, 0*femtometer,
     true, false, Interface::lowerlim);

    static Parameter<StringFragmentation,double> interfaceRCutOff
    ("RCutOff",
     "Cutoff radius at which we dont let strings shove each"
     "other around anymore -- given as a multiple of StringR0",
     &StringFragmentation::rCutOff, 0, 4.0,
     0.0, 0,
     true, false, Interface::lowerlim);

    static Parameter<StringFragmentation,StringTension> interfaceGAmplitude
    ("GAmplitude",
     "Amplitude of shoving energy density function, should be approx."
     "the average effective string tension",
     &StringFragmentation::gAmplitude, GeV/femtometer, 1.0*GeV/femtometer,
     0.0*GeV/femtometer, 0*GeV/femtometer,
     true, false, Interface::lowerlim);

    static Parameter<StringFragmentation,double> interfaceGExponent
    ("GExponent",
     "Parameter to divide the exponent of energy density function for"
     "shoving.",
     &StringFragmentation::gExponent, 0, 10.0,
     0.0, 0,
     true, false, Interface::lowerlim);


    static Parameter<StringFragmentation,double> interfaceYCutOff
    ("YCutOff",
     "Cutoff rapidity for excitation clustering. This is the minimal"
     "effective rapidity span for a dipole after clustering",
     &StringFragmentation::yCutOff, 0, 5.0,
     0.0, 0,
     true, false, Interface::lowerlim);

    static Switch<StringFragmentation,bool> interfaceLorentz
    ("Lorentz",
     "Do Lorentz contraction of string fields moving with a transverse velocity",
     &StringFragmentation::lorentz, true, true, false);
    static SwitchOption interfaceLorentzTrue
    (interfaceLorentz,"True","enabled.",true);
    static SwitchOption interfaceLorentzFalse
    (interfaceLorentz,"False","disabled.",false);


    static Switch<StringFragmentation,bool> interfaceCylinder
    ("Cylinder",
     "Do Cylinder Area Shoving",
     &StringFragmentation::cylinder, false, true, false);
    static SwitchOption interfaceCylinderTrue
    (interfaceCylinder,"True","enabled.",true);
    static SwitchOption interfaceCylinderFalse
    (interfaceCylinder,"False","disabled.",false);

    static Switch<StringFragmentation,bool> interfaceWindow
    ("StringWindow",
     "Enable the 'window'-cut procedure, off by default."
     "Parameters will only affect if this is switched on. ",
     &StringFragmentation::window, false, true, false);
    static SwitchOption interfaceWindowTrue
    (interfaceWindow,"True","enabled.",true);
    static SwitchOption interfaceWindowFalse
    (interfaceWindow,"False","disabled.",false);

    static Parameter<StringFragmentation,Energy> interfaceStringpTCut
    ("StringpTCut",
     "Strings with a constituent pT higher than StringpTCut (GeV), will not participate "
     " in the ropewalk",
     &StringFragmentation::stringpTCut, GeV, 3.0*GeV,
     0.0*GeV, 0*GeV,
     true, false, Interface::lowerlim);

    static Parameter<StringFragmentation,double> interfaceMinStringy
    ("MinStringy",
     "String smaller than MinStringy (in rapidity) will not participate "
     " in the ropewalk",
     &StringFragmentation::minStringy, 0, 8.0,
     0.0, 0,
     true, false, Interface::lowerlim);

    static Switch<StringFragmentation,bool> interfaceLongSoft
    ("LongSoft",
     "Only do ropewalk with long and soft strings, set cuts accordingly",
     &StringFragmentation::longSoft, false, true, false);
    static SwitchOption interfaceLongSoftTrue
    (interfaceLongSoft,"True","enabled.",true);
    static SwitchOption interfaceLongSoftFalse
    (interfaceLongSoft,"False","disabled.",false);


    static Switch<StringFragmentation,bool> interfaceLongStringsOnly
    ("LongStringsOnly",
     "Hadronize only long and soft strings, no spectators, set cuts accordingly",
     &StringFragmentation::longStringsOnly, false, true, false);
    static SwitchOption interfaceLongStringsOnlyTrue
    (interfaceLongStringsOnly,"True","enabled.",true);
    static SwitchOption interfaceLongStringsOnlyFalse
    (interfaceLongStringsOnly,"False","disabled.",false);


    static Parameter<StringFragmentation,Energy> interfaceStringm0
    ("Stringm0",
     ".",
     &StringFragmentation::stringm0, GeV, 1.0*GeV,
     0.0*GeV, 0*GeV,
     true, false, Interface::lowerlim);

    static Parameter<StringFragmentation,double> interfaceJunctionDiquark
    ("JunctionDiquark",
     "Suppress diquark production in breaking of junctions with this amount",
     &StringFragmentation::junctionDiquark, 0, 1.0,
     0.0, 0,
     true, false, Interface::lowerlim);

    static Parameter<StringFragmentation,double> interfaceAlpha
    ("Alpha",
     "Boost the enhanced string tension with additional factor",
     &StringFragmentation::alpha, 0, 1.0,
     1.0, 0,
     true, false, Interface::lowerlim);

    static Switch<StringFragmentation,bool> interfaceAverage
    ("Average",
     "Disable to try and hadronise strings with individual tensions"
     "instead of average value.",
     &StringFragmentation::average, false, true, false);
    static SwitchOption interfaceAverageTrue
    (interfaceAverage,"True","enabled.",true);
    static SwitchOption interfaceAverageFalse
    (interfaceAverage,"False","disabled.",false);

    static Parameter<StringFragmentation,double> interfaceStringyCut
    ("StringyCut",
     "No enhancement of strings with a constituent pT higher than StringpTCut (GeV), and within "
     " StringyCut.",
     &StringFragmentation::stringyCut, 0, 2.0,
     0.0, 0,
     true, false, Interface::lowerlim);

    static Parameter<StringFragmentation,Length> interfaceStringR0
    ("StringR0",
     "In the string overlap model the string R0 dictates the minimum radius "
     " of a string (in fm).",
     &StringFragmentation::stringR0, femtometer, 0.5*femtometer,
     0.0*femtometer, 0*femtometer,
     true, false, Interface::lowerlim);

    static Parameter<StringFragmentation,double> interfaceFragmentationMass
    ("FragmentationMass",
     "Set the mass used for the f(z) integral in the overlap string model "
     " default is pion mass (units GeV).",
     &StringFragmentation::fragmentationMass, 0, 0.135,
     0., 1.,
     true, false, Interface::lowerlim);

    static Parameter<StringFragmentation,double> interfaceBaryonSuppression
    ("BaryonSuppression",
     "Set the fudge factor used for baryon suppression in the overlap string model. "
     " One day this should be calculated properly. This day is not today. Probably around 2...",
     &StringFragmentation::baryonSuppression, 0, 0.5,
     0.0, 1.0,
     true, false, Interface::limited);

    static Parameter<StringFragmentation,int> interfaceMaxTries
    ("MaxTries",
     "Sometimes Pythia gives up on an event too easily. We therefore allow "
     "it to re-try a couple of times.",
     &StringFragmentation::maxTries, 2, 1, 0,
     true, false, Interface::lowerlim);

}

std::string StringFragmentation::convert(double d) {
    std::ostringstream os;
    os << d;
    return os.str();
}
